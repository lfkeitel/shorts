package main

import (
	"flag"
	"log"
)

func main() {
	port := flag.String("p", "8080", "Port number")
	address := flag.String("a", "127.0.0.1", "Address to bind")
	dbPath := flag.String("db", "database.db", "Database path")

	db, err := openDatabase(*dbPath)
	if err != nil {
		log.Fatal(err)
	}

	if err := runServer(*address, *port, db); err != nil {
		log.Fatal(err)
	}
}
