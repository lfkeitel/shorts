package main

import (
	"fmt"

	bolt "github.com/coreos/bbolt"
)

const (
	bktShortLinks = "shortLinks"
)

var (
	buckets = [][]byte{
		[]byte(bktShortLinks),
	}
)

type database struct {
	db *bolt.DB
}

func openDatabase(path string) (*database, error) {
	db, err := bolt.Open(path, 0600, nil)
	if err != nil {
		return nil, err
	}

	err = db.Update(func(tx *bolt.Tx) error {
		for _, bkt := range buckets {
			_, err := tx.CreateBucketIfNotExists(bkt)
			if err != nil {
				return fmt.Errorf("create bucket: %s", err)
			}
		}
		return nil
	})
	if err != nil {
		db.Close()
		return nil, err
	}

	return &database{db: db}, nil
}

func (db *database) close() error {
	return db.db.Close()
}

func (db *database) lookup(bucket, key string) ([]byte, error) {
	var answer []byte

	err := db.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucket))
		if b == nil {
			return fmt.Errorf("bucket %s doesn't exist", bucket)
		}

		v := b.Get([]byte(key))
		if v == nil {
			return nil
		}

		answer = make([]byte, len(v))
		copy(answer, v)
		return nil
	})

	return answer, err
}

func (db *database) put(bucket, key string, value []byte) error {
	return db.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucket))
		if b == nil {
			return fmt.Errorf("bucket %s doesn't exist", bucket)
		}

		return b.Put([]byte(key), value)
	})
}

func (db *database) delete(bucket, key string) error {
	return db.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucket))
		if b == nil {
			return fmt.Errorf("bucket %s doesn't exist", bucket)
		}

		return b.Delete([]byte(key))
	})
}
