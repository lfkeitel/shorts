package main

import (
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	bolt "github.com/coreos/bbolt"
)

type handler func(http.ResponseWriter, *http.Request, *database)

var (
	defaultRoute handler = shortLinkHandler
	tmpls                = template.Must(template.ParseGlob("templates/*.tmpl"))

	routes = map[string]handler{
		"":  rootHandler,
		"_": cmdHandler,
	}
)

func runServer(address, port string, db *database) error {
	http.HandleFunc("/", router(db))

	bind := fmt.Sprintf("%s:%s", address, port)
	fmt.Printf("Listening on http://%s\n", bind)

	return http.ListenAndServe(bind, nil)
}

func router(db *database) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		urlParts := strings.Split(r.URL.Path, "/")[1:]

		route, exists := routes[urlParts[0]]
		if exists {
			route(w, r, db)
			return
		}

		defaultRoute(w, r, db)
	}
}

func rootHandler(w http.ResponseWriter, r *http.Request, db *database) {
	tmpls.ExecuteTemplate(w, "index", nil)
}

func cmdHandler(w http.ResponseWriter, r *http.Request, db *database) {
	urlParts := strings.Split(r.URL.Path, "/")[1:]
	if len(urlParts) != 2 {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Command not found"))
		return
	}

	cmd := urlParts[1]
	switch cmd {
	case "savecode":
		cmdSaveCode(w, r, db)
	case "backup":
		cmdBackupDB(w, r, db)
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Command not found"))
	}
}

var shortCodeRegex = regexp.MustCompile(`^[a-zA-Z0-9][a-zA-Z0-9_]*$`)

func cmdSaveCode(w http.ResponseWriter, r *http.Request, db *database) {
	srcURL := strings.TrimSpace(r.FormValue("src-url"))
	code := strings.TrimSpace(r.FormValue("s-code"))
	if code == "" {
		code = randStr(6)
	}
	if !shortCodeRegex.MatchString(code) {
		renderHomeMessage(w, "Invalid shortcode")
		return
	}

	exists, err := checkShortLinkExists(db, code)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err.Error())
		return
	}
	if exists {
		renderHomeMessage(w, fmt.Sprintf("Shortcode %s already exists", code))
		return
	}

	err = db.put(bktShortLinks, code, []byte(srcURL))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err.Error())
		return
	}

	fmt.Printf("Shortcode created '%s' -> '%s' from %s\n", code, srcURL, r.RemoteAddr)
	link := fmt.Sprintf("http://%s/%s", r.Host, code)
	renderHomeMessage(w, fmt.Sprintf("Shortcode created successfully: <a href=\"%s\">%s</a>", link, link))
}

func renderHomeMessage(w io.Writer, msg string) {
	tmpls.ExecuteTemplate(w, "index", struct {
		Message template.HTML
	}{
		Message: template.HTML(msg),
	})
}

func checkShortLinkExists(db *database, code string) (bool, error) {
	redirect, err := db.lookup(bktShortLinks, code)
	if err != nil {
		return false, err
	}
	return (redirect != nil), nil
}

func cmdBackupDB(w http.ResponseWriter, r *http.Request, db *database) {
	err := db.db.View(func(tx *bolt.Tx) error {
		w.Header().Set("Content-Type", "application/octet-stream")
		w.Header().Set("Content-Disposition", `attachment; filename="database.db"`)
		w.Header().Set("Content-Length", strconv.Itoa(int(tx.Size())))
		_, err := tx.WriteTo(w)
		return err
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err.Error())
	}
}

func shortLinkHandler(w http.ResponseWriter, r *http.Request, db *database) {
	urlParts := strings.Split(r.URL.Path, "/")[1:]
	if len(urlParts) > 1 {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Shortcode doesn't exist"))
		return
	}

	code := urlParts[0]
	redirect, err := db.lookup(bktShortLinks, code)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err.Error())
		return
	}

	if redirect == nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(fmt.Sprintf("Shortcode '%s' doesn't exist", code)))
		return
	}

	if r.Method == http.MethodDelete {
		if err := db.delete(bktShortLinks, code); err != nil {
			log.Println(err)
		}
		fmt.Printf("Shortcode '%s' deleted from %s\n", code, r.RemoteAddr)
	} else {
		fmt.Printf("Shortcode lookup '%s' from %s\n", code, r.RemoteAddr)
		http.Redirect(w, r, string(redirect), http.StatusTemporaryRedirect)
	}
}
