# shorts

Shorts is a URL shortener application. Give it a URL and a code (or let it generate
one) and use it in place of the full URL.
